﻿using UnityEngine;

public class PickUpNormalPad : MonoBehaviour
{
    private Pad _pad;

    private void Start() {
        _pad = FindObjectOfType<Pad>();
    }


    private void OnTriggerEnter2D(Collider2D collision) {
        //   if (collision.CompareTag("Pad"))
        if(collision.tag == "Pad") {
            _pad.transform.localScale = new Vector2(1F, 0.5f);
        }
    }
}
