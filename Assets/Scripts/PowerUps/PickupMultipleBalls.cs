﻿using UnityEngine;

public class PickupMultipleBalls : MonoBehaviour
{
    [SerializeField] int ballsNumber = 2;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pad")
        {
            Ball ball = FindObjectOfType<Ball>();
            for (int i = 0; i < ballsNumber; i++)
            {
                Ball newBall = Instantiate(ball);
                Vector2 ballPos = newBall.transform.position;
                // newBall.transform.position = new Vector3(ballPos.x +i, ballPos.y +i);
                newBall.LaunchBall();
            }

            Destroy(gameObject);
        }
    }
}
