﻿using UnityEngine;

public class PickIpSpeedBall : MonoBehaviour
{
    private Ball _ball;

    private void Start() {
        _ball = FindObjectOfType<Ball>();
    }


    private void OnTriggerEnter2D(Collider2D collision) {
        //   if (collision.CompareTag("Pad"))
        if(collision.tag == "Pad") {
           _ball.Speed2x();
        }
    }
}
