﻿using UnityEngine;

public class PickUpLong : MonoBehaviour{
    private Pad _pad;

    private void Start() {
        _pad = FindObjectOfType<Pad>();
    }


    private void OnTriggerEnter2D(Collider2D collision) {
        //   if (collision.CompareTag("Pad"))
        if(collision.tag == "Pad") {
            _pad.transform.localScale = new Vector2(2F, 0.5f);
        }
    }
}
