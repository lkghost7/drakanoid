﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pad : MonoBehaviour
{
    [SerializeField] float minX = 1.2f;
    [SerializeField] float maxX = 14.8f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Get mouse position in pixels
        Vector3 mousePosInPixels = Input.mousePosition;
        //Cast pixels position to World Unit positions
        Vector3 mousePosInUnits = Camera.main.ScreenToWorldPoint(mousePosInPixels);

        float newX = mousePosInUnits.x;
        //clamp X in range of minX and maxX
        newX = Mathf.Clamp(newX, minX, maxX);
        //Create new pad position value
        Vector2 newPadPos = new Vector2(newX, transform.position.y);
        //set new pad position to GameObject's Transform component
        transform.position = newPadPos;
    }
}
