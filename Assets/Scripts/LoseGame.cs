﻿using UnityEngine;

public class LoseGame : MonoBehaviour {
    GameManager gameManager;
    Ball        ball;

    private void Start() {
        gameManager = FindObjectOfType<GameManager>();
        ball        = FindObjectOfType<Ball>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.CompareTag("Ball")) {
            Debug.Log("OnTriggerEnter2D");
            Destroy(collision.gameObject);
            CheckCount.Instance._CountsBall--;
            if(CheckCount.Instance._CountsBall <= 0) {
                Invoke("Restart", 0.3f);
            }
        }
    }

    void Restart() {
         Debug.Log("Restart");
        gameManager.LoseLive();
        ball.RestartBall();
    }
}
