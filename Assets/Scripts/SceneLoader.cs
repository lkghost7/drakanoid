﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
    {
        
        //public int variable;

        // Start is called before the first frame update
        void Start()
        {
            //Debug.Log("Start: " + variable);
           // DontDestroyOnLoad(gameObject);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ExitGame();
            }
        }

        private void OnDestroy()
        {
           // Debug.Log("Destroy: " + variable);
        }

        public void LoadNextScene()
        {
            Scene activeScene = SceneManager.GetActiveScene();
            int currentSceneIndex = activeScene.buildIndex;
            SceneManager.LoadScene(currentSceneIndex + 1);
        }

        public void LoadNextSceneWithDelay()
        {
            Invoke("LoadNextScene", 0.2f);
        }

        public void LoadFirstScene()
        {
            SceneManager.LoadScene(0);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
